import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // app.useGlobalPipes(new ValidationPipe({transform:true}));
  // app.useGlobalPipes(new ValidationPipe({skipMissingProperties:false}));
  app.useGlobalPipes(new ValidationPipe({whitelist:true}));
  // app.useGlobalPipes(new ValidationPipe({forbidNonWhitelisted:true}));
  // app.useGlobalPipes(new ValidationPipe({forbidUnknownValues:true}));
  // app.useGlobalPipes(new ValidationPipe({disableErrorMessages:true}));
  // app.useGlobalPipes(new ValidationPipe({dismissDefaultMessages:true}));
  // app.useGlobalPipes(new ValidationPipe());
  await app.listen(3000);
}
bootstrap();
