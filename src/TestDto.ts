import { IsInt, IsString } from 'class-validator';




export class TestValidationDto {
  // @IsString({ message: 'input must be a string' })
  @IsString()
   name:string;
  // @IsInt({ message: 'input should be a number' }) 
  @IsInt() 
  age: number;
}

