import { TestValidationDto } from './TestDto';
import { Controller, Get, Body, Post, ValidationPipe, UsePipes, ParseIntPipe, Query, ParseBoolPipe, Param, ParseArrayPipe } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  dataset: TestValidationDto[] = [];

  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
  @Get(':id')
  findOne(@Param('id',ParseIntPipe) id:number,
  // @Query('sort',ParseBoolPipe) sort:boolean):string
  @Query('id',new ParseArrayPipe({items:Number,separator:' ,'})) ids:number[]):any
{
  // console.log(typeof id,typeof sort);
  console.log(ids);
  // return `the id is ${id} with the query parameter ${sort}`;
  return `the id is ${id} with the query parameter ${ids}`;
}
  @Post()
  // @UsePipes(new ValidationPipe({transform:true}))
  // @UsePipes(new ValidationPipe({skipMissingProperties:false}))
  // @UsePipes(new ValidationPipe({whitelist:true})) // will strip out the additional keys
 //this one is not working @UsePipes(new ValidationPipe({forbidNonWhitelisted:true})) // will throw error for additional keys
 
//  @UsePipes(new ValidationPipe({forbidUnknownValues:true})) not working
//  @UsePipes(new ValidationPipe({disableErrorMessages:true})) // error messages will be disabled
//  @UsePipes(new ValidationPipe({errorHttpStatusCode:500})) // set http status code
//  @UsePipes(new ValidationPipe({dismissDefaultMessages:true})) // removes default err message

 
 
 
 createTest(@Body() userData: TestValidationDto): string {
 
    this.dataset.push(userData);
    console.log(this.dataset);
    return 'your data is totally valid';
  }
}
